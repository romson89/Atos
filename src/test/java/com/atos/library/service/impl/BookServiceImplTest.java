package com.atos.library.service.impl;

import com.atos.library.exception.BookAlreadyLentException;
import com.atos.library.model.Book;
import com.atos.library.model.User;
import com.atos.library.repository.BookRepository;
import com.atos.library.repository.UserRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Created by roman on 19.04.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class BookServiceImplTest {

    @InjectMocks
    BookServiceImpl bookService;

    @Mock
    UserRepository userRepository;

    @Mock
    BookRepository bookRepository;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private Book bookToWhomTheBellTolls = new Book();
    private long idToWhomTheBellTolls = 1L;
    private String titleToWhomTheBellTolls = "For Whom the Bell Tolls";
    private String authorToWhomTheBellTolls = "Ernest Hemingway";
    private int yearToWhomTheBellTolls = 1940;

    private Book bookKoniecSwiataWBreslau = new Book();
    private long idKoniecSwiataWBreslau = 2L;
    private String titleKoniecSwiataWBreslau = "Koniec swiata w Breslau";
    private String authorKoniecSwiataWBreslau = "Marek Krajewski";
    private int yearKoniecSwiataWBreslau = 2003;

    private Book bookPrzedwiosnie = new Book();
    private long idPrzedwiosnie = 3L;
    private String titlePrzedwiosnie = "Przedwiosnie";
    private String authorPrzedwiosnie = "Stefan Zeromski";
    private int yearPrzedwiosnie = 1924;

    private Book bookToHaveAndToHaveNot = new Book();
    private long idToHaveAndToHaveNot = 4L;
    private String titleToHaveAndToHaveNot = "To Have and to have not";
    private int yearToHaveAndToHaveNot = 1937;

    private User userJohnDoe = new User();
    private String nameJohnDoe = "John Doe";
    private long idJohnDoe = 12345678901L;

    private List<Book> books;

    @Before
    public void setUp() {
        books = prepareBooks();
    }

    @Test
    public void shouldNotAddBookWithEmptyTitleAndThrowException() {
        //given
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("No title provided.");

        //when
        bookService.addNewBook(null, authorToWhomTheBellTolls, yearToWhomTheBellTolls);

        //then
    }

    @Test
    public void shouldNotAddBookWithEmptyAuthorAndThrowException() {
        //given
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("No author provided.");

        //when
        bookService.addNewBook(titleToWhomTheBellTolls, null, yearToWhomTheBellTolls);

        //then
    }

    @Test
    public void shouldNotAddBookWithEmptyYearAndThrowException() {
        //given
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("No year provided.");

        //when
        bookService.addNewBook(titleToWhomTheBellTolls, authorToWhomTheBellTolls, null);

        //then
    }

    @Test
    public void shouldAddBook() {
        //given

        //when
        bookService.addNewBook(titleToWhomTheBellTolls, authorToWhomTheBellTolls, yearToWhomTheBellTolls);

        //then
        verify(bookRepository).save(any(Book.class));
    }

    @Test
    public void shouldInvokeDeleteBook() {
        //given

        //when
        bookService.removeBook(idKoniecSwiataWBreslau);

        //then
        verify(bookRepository).delete(eq(idKoniecSwiataWBreslau));
    }

    @Test
    public void shouldListAllBooks() throws IOException {
        //given
        when(bookRepository.findAll()).thenReturn(books);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        String expectedOutput = prepareExpectedAllBooksListing();

        //when
        bookService.listAllBooks();
        outputStream.flush();

        //then
        String outputText = new String(outputStream.toByteArray());
        assertTrue(outputText.contains(expectedOutput));
    }

    @Test
    public void shouldNotSearchBookWithEmptyCriteriaAndThrowException() {
        //given
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("At least one search criteria must be specified.");

        //when
        bookService.search(null, null, null);

        //then
    }

    @Test
    public void shouldNotListAnyBooksForEmptyResultSet() throws IOException {
        //given
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        String notExistingTitle = "NotExistingTitle";
        String notExistingAuthor = "NotExistingAuthor";
        int totallyFutureYear = 2222;

        //when
        List<Book> searchResult = bookService.search(notExistingTitle, notExistingAuthor, totallyFutureYear);
        outputStream.flush();

        //then
        String outputText = new String(outputStream.toByteArray());
        assertTrue(outputText.contains("No books found for title: " + notExistingTitle + ", author: " + notExistingAuthor + ", year: " + totallyFutureYear + "."));
        assertTrue(searchResult.isEmpty());
    }

    @Test
    public void shouldListTwoErnestHemingwayBooks() throws IOException {
        //given
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        List<Book> hemingwayBooks = new ArrayList<>();
        hemingwayBooks.add(bookToHaveAndToHaveNot);
        hemingwayBooks.add(bookToWhomTheBellTolls);
        when(bookRepository.findByTitleOrAuthorOrYear(anyObject(), eq(authorToWhomTheBellTolls), anyObject()))
                .thenReturn(hemingwayBooks);
        String expectedOutput = prepareExpectedHemingwayBooksListing();

        //when
        List<Book> searchResult = bookService.search(null, authorToWhomTheBellTolls, null);
        outputStream.flush();

        //then
        String outputText = new String(outputStream.toByteArray());
        assertTrue(outputText.contains(expectedOutput));
        assertEquals(2, searchResult.size());
    }

    @Test
    public void shouldNotLendNotExistingBookAndThrowException() throws BookAlreadyLentException {
        //given
        long id = 100L;
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("There is no book with id=100");

        //when
        bookService.lend(id, "blah");

        //then
    }

    @Test
    public void shouldNotLendAlreadyLentBookAndThrowException() throws BookAlreadyLentException {
        //given
        expectedException.expect(BookAlreadyLentException.class);
        expectedException.expectMessage("Book with id=" + idKoniecSwiataWBreslau + " is already lent by " + userJohnDoe + ".");
        when(bookRepository.findOne(idKoniecSwiataWBreslau)).thenReturn(bookKoniecSwiataWBreslau);

        //when
        bookService.lend(idKoniecSwiataWBreslau, "blah");

        //then
    }

    @Test
    public void shouldNotLendBookToNotExistingUserAndThrowException() throws BookAlreadyLentException {
        //given
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("There is no user: blah");
        when(bookRepository.findOne(idToWhomTheBellTolls)).thenReturn(bookToWhomTheBellTolls);

        //when
        bookService.lend(idToWhomTheBellTolls, "blah");

        //then
    }

    @Test
    public void shouldLendNotLentBookToExistingUser() throws BookAlreadyLentException {
        //given
        when(bookRepository.findOne(idToWhomTheBellTolls)).thenReturn(bookToWhomTheBellTolls);
        when(userRepository.findOneByName(nameJohnDoe)).thenReturn(userJohnDoe);

        //when
        bookService.lend(idToWhomTheBellTolls, nameJohnDoe);

        //then
        assertEquals(userJohnDoe, bookToWhomTheBellTolls.getOwner());
        assertTrue(userJohnDoe.getBooks().contains(bookToWhomTheBellTolls));
    }

    @Test
    public void shouldNotPrintDetailsOfBookWithEmptyIdAndThrowException() {
        //given
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Book id cannot be null.");

        //when
        bookService.showDetails(null);

        //then
    }

    @Test
    public void shouldNotPrintDetailsOfNotExistingBook() throws IOException {
        //given
        when(bookRepository.findOne(idKoniecSwiataWBreslau)).thenReturn(bookKoniecSwiataWBreslau);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        long id = 101L;

        //when
        bookService.showDetails(id);
        outputStream.flush();

        //then
        String output = new String(outputStream.toByteArray());
        assertTrue(output.contains("There is no such book"));
    }

    @Test
    public void shouldPrintDetailsOfExistingBook() throws IOException {
        //given
        when(bookRepository.findOne(idKoniecSwiataWBreslau)).thenReturn(bookKoniecSwiataWBreslau);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        //when
        bookService.showDetails(idKoniecSwiataWBreslau);
        outputStream.flush();

        //then
        String output = new String(outputStream.toByteArray());
        assertTrue(output.contains(bookKoniecSwiataWBreslau.toString()));
    }

    private List<Book> prepareBooks() {
        List<Book> books = new ArrayList<>();

        userJohnDoe.setName(nameJohnDoe);
        userJohnDoe.setUserId(idJohnDoe);

        bookToWhomTheBellTolls.setTitle(titleToWhomTheBellTolls);
        bookToWhomTheBellTolls.setAuthor(authorToWhomTheBellTolls);
        bookToWhomTheBellTolls.setYear(yearToWhomTheBellTolls);
        bookToWhomTheBellTolls.setId(idToWhomTheBellTolls);
        books.add(bookToWhomTheBellTolls);

        bookKoniecSwiataWBreslau.setTitle(titleKoniecSwiataWBreslau);
        bookKoniecSwiataWBreslau.setAuthor(authorKoniecSwiataWBreslau);
        bookKoniecSwiataWBreslau.setYear(yearKoniecSwiataWBreslau);
        bookKoniecSwiataWBreslau.setId(idKoniecSwiataWBreslau);
        bookKoniecSwiataWBreslau.setOwner(userJohnDoe);
        userJohnDoe.getBooks().add(bookKoniecSwiataWBreslau);
        books.add(bookKoniecSwiataWBreslau);

        bookPrzedwiosnie.setTitle(titlePrzedwiosnie);
        bookPrzedwiosnie.setAuthor(authorPrzedwiosnie);
        bookPrzedwiosnie.setYear(yearPrzedwiosnie);
        bookPrzedwiosnie.setId(idPrzedwiosnie);
        books.add(bookPrzedwiosnie);

        bookToHaveAndToHaveNot.setTitle(titleToHaveAndToHaveNot);
        bookToHaveAndToHaveNot.setAuthor(authorToWhomTheBellTolls);
        bookToHaveAndToHaveNot.setYear(yearToHaveAndToHaveNot);
        bookToHaveAndToHaveNot.setId(idToHaveAndToHaveNot);
        books.add(bookToHaveAndToHaveNot);

        return books;
    }

    private String prepareExpectedAllBooksListing() {
        return bookToWhomTheBellTolls + "\r\n" + bookKoniecSwiataWBreslau + "\r\n" + bookPrzedwiosnie + "\r\n" +
                bookToHaveAndToHaveNot + "\r\n======================\r\nLent books: 1\r\nFree books: 3\r\n";
    }

    private String prepareExpectedHemingwayBooksListing() {
        return "List of books found for title: null, author: " + authorToWhomTheBellTolls+ ", year: " + null + ".\r\n" +
                bookToHaveAndToHaveNot + "\r\n" + bookToWhomTheBellTolls;
    }
}