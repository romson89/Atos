package com.atos.library.service;

import com.atos.library.exception.BookAlreadyLentException;
import com.atos.library.model.Book;

import java.util.List;

/**
 * Created by roman on 19.04.17.
 */
public interface BookService {
    void addNewBook(String title, String author, Integer year);

    void removeBook(Long id);

    void listAllBooks();

    List<Book> search(String title, String author, Integer year);

    void lend(Long id, String userName) throws BookAlreadyLentException;

    void showDetails(Long id);
}
