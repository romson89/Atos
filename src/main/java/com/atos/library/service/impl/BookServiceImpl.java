package com.atos.library.service.impl;

import com.atos.library.exception.BookAlreadyLentException;
import com.atos.library.model.Book;
import com.atos.library.model.User;
import com.atos.library.repository.BookRepository;
import com.atos.library.repository.UserRepository;
import com.atos.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by roman on 19.04.17.
 */
@Service
public class BookServiceImpl implements BookService{
    private final BookRepository bookRepository;
    private final UserRepository userRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository, UserRepository userRepository) {
        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public void addNewBook(String title, String author, Integer year) {
        Assert.hasText(title, "No title provided.");
        Assert.hasText(author, "No author provided.");
        Assert.notNull(year, "No year provided.");

        Book book = new Book();
        book.setTitle(title);
        book.setAuthor(author);
        book.setYear(year);

        bookRepository.save(book);
    }

    public void removeBook(Long id) {
        bookRepository.delete(id);
    }

    public void listAllBooks() {
        List<Book> books = bookRepository.findAll();

        books.forEach(System.out::println);

        long lentBooks = books
                .stream()
                .filter(book -> book.getOwner()!=null)
                .count();

        long freeBooks = books.size() - lentBooks;

        System.out.println("======================");
        System.out.println("Lent books: " + lentBooks);
        System.out.println("Free books: " + freeBooks);
    }

    public List<Book> search(String title, String author, Integer year) {
        if(title==null && author==null && year==null) {
            throw new IllegalArgumentException("At least one search criteria must be specified.");
        }

        List<Book> searchResult = bookRepository.findByTitleOrAuthorOrYear(title, author, year);

        if(searchResult.isEmpty()) {
            System.out.println("No books found for title: " + title + ", author: " + author + ", year: " + year + ".");
        }
        else {
            System.out.println("List of books found for title: " + title + ", author: " + author + ", year: " + year + ".");
            searchResult.forEach(System.out::println);
        }

        return searchResult;
    }

    @Transactional
    public void lend(Long id, String userName) throws BookAlreadyLentException{
        Book book = bookRepository.findOne(id);
        Assert.notNull(book, "There is no book with id=" + id + ".");

        if(book.getOwner()!=null) {
            throw new BookAlreadyLentException(book);
        }

        User user = userRepository.findOneByName(userName);
        Assert.notNull(user, "There is no user: " + userName);

        user.getBooks().add(book);
        book.setOwner(user);

        bookRepository.save(book);
        userRepository.save(user);
    }

    public void showDetails(Long id) {
        Assert.notNull(id, "Book id cannot be null.");

        Book book = bookRepository.findOne(id);

        if(book==null) {
            System.out.println("There is no such book");
        }
        else {
            System.out.println(book);
        }
    }
}
