package com.atos.library.repository;

import com.atos.library.model.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by roman on 19.04.17.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    User findOneByName(String name);
}
