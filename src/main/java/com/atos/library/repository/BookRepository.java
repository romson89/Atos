package com.atos.library.repository;

import com.atos.library.model.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by roman on 19.04.17.
 */
public interface BookRepository extends CrudRepository<Book, Long> {
    List<Book> findAll();

    List<Book> findByTitleOrAuthorOrYear(String title, String author, Integer year);
}
