package com.atos.library.exception;

import com.atos.library.model.Book;

/**
 * Created by roman on 19.04.17.
 */
public class BookAlreadyLentException extends Exception{

    public BookAlreadyLentException(Book book) {
        super("Book with id=" + book.getId() + " is already lent by " + book.getOwner() + ".");
    }
}
